/* 
 ** Player choses the top left color  
 ** If any adjacent square is the same color, it must be added to the caught squares, and any adjacent to it the same color 
 ** Then when the top left color is changed again, all caught squares must update
 */

/* Global Vars */
var debug = 0;
var mGroupedCells = [];
var gridSize = 12;
var turns;
var oGrid;
var oButtons;
var lastRow = 0;
var colorCount = 0;
var lastColor;
var stopRepeat;

/* Color Schemes */
/* Basic */
var purple = ["purple", "rgb(106, 105, 173)"];
var blue = ["blue", "rgb(80, 177, 219)"];
var green = ["green", "rgb(142, 171, 45)"];
var yellow = ["yellow", "rgb(251, 244, 50)"];
var red = ["red", "rgb(216, 83, 42)"];
var pink = ["pink", "rgb(226, 128, 169)"];

/* Scheme 2 */
var pale_green = ["pale_green", "rgb(185, 242, 145)"];
var teal = ["teal", "rgb(80, 191, 148)"];
var charcoal = ["charcoal", "rgb(94, 88, 88)"];
var coral = ["coral", "rgb(249, 35, 73)"];
var orange = ["orange", "rgb(255, 131, 93)"];
var banana = ["banana", "rgb(246, 220, 133)"];

var colors = [purple, blue, green, yellow, red, pink];
//var colors = [pale_green, teal, charcoal, coral, orange, banana];

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// GENERIC FUNCTIONS ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/*
 ** unique(set)
 ** Returns unique member of a set
 */
function unique(x) {
  "use strict";
  return x.filter(function (elem, index) {
    return x.indexOf(elem) == index;
  });
};

/*
 ** unionArrays(array1, array2)
 ** Returns the union of two arrays
 */
function unionArrays(x, y) {
  "use strict";
  return unique(x.concat(y));
};

/*
 ** frequency(array)
 ** Returns an array counting the frequency of each element within
 */
function frequency(arr) {
  "use strict";
  var i,
    a = [],
    b = [],
    prev;
  arr.sort();
  for (i = 0; i < arr.length; i++) {
    if (arr[i] != prev) {
      a.push(arr[i]);
      b.push(1);
    } else {
      b[b.length - 1]++;
    }
    prev = arr[i];
  }
  return [a, b];
}

/* 
 ** randomChoice(array)
 ** Returns a randomly selected array element
 */
function randomChoice(arr) {
  "use strict";
  return arr[Math.floor(arr.length * Math.random())];
}

/* 
 ** findGroup(element, array_of_arrays)
 ** Returns index of array containing chosen element
 ** within array of arrays
 */
function findGroup(elem, arrayOfArrays) {
  "use strict";
  var idx, foundIdx;
  foundIdx = -1;
  for (idx = 0; idx < arrayOfArrays.length; idx++) {
    /* if the element's index is included in the current array */
    if (arrayOfArrays[idx].indexOf(elem) != -1) {
      /* return the array's index */
      foundIdx = idx;
    }
  }
  return foundIdx;
}

/*
 ** findColor(color, array_of_arrays)
 ** Returns index of array containing chosen element
 ** within array of arrays
 */
function findColor(color, arrayOfArrays) {
  "use strict";
  var idx, foundIdx;
  foundIdx = -1;
  for (idx = 0; idx < arrayOfArrays.length; idx++) {
    if (arrayOfArrays[idx][0].dataset.color == color) {
      foundIdx = idx;
    }
  }
  return foundIdx;
}

/* 
 ** searchArray(cell, array)
 ** Returns true if an array contains a cell
 */
function searchArray(cell, array) {
  "use strict";
  if (array.indexOf(cell) != -1) {
    return true;
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// AUTOSOLVERS //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/*
 ** step()
 ** Performs a single step of chosen autosolver
 */
function step() {
  "use strict"
  var fn, i;
  var buttons = ["autosolve_groups", "autosolve_cycle", "autosolve_neighbour"];
  for (i = 0; i < buttons.length; i++) {
    if (!document.getElementById(buttons[i]).disabled) {
      console.log(buttons[i]);
      window[buttons[i]]();
    }
  }
}

/*
 ** repeat(function, element_identifier)
 ** Repeatedly calls chosen autosolver function
 */
function repeat(fn, elemID) {
  "use strict";
  var txt = document.getElementById(elemID).innerHTML;
  var interval = 100;
  if (elemID == "autosolve_neighbour") {
    document.getElementById("neighbour_txt").style.display = "block";
    document.getElementById("autosolve_groups").disabled = true;
    document.getElementById("autosolve_cycle").disabled = true;
  } else if (elemID == "autosolve_groups") {
    document.getElementById("groups_txt").style.display = "block";
    document.getElementById("autosolve_neighbour").disabled = true;
    document.getElementById("autosolve_cycle").disabled = true;
  } else if (elemID == "autosolve_cycle") {
    document.getElementById("cycle_txt").style.display = "block";
    document.getElementById("autosolve_groups").disabled = true;
    document.getElementById("autosolve_neighbour").disabled = true;
  }

  stopFn(fn, txt, elemID, true);

  (function again() {
    fn();
    if (colorCount < 144 && !stopRepeat) {
      setTimeout(again, interval);
    }
  })();
  console.log(stopRepeat);
}

/*
 ** stopFn(function, text, element_identifier, visible_boolean)
 ** Changes autosolve button functions based on stop / start
 */
function stopFn(fn, txt, elemID, visible) {
  "use strict";
  if (visible == true) {
    stopRepeat = false;
    document.getElementById("step").style.visibility = "hidden";
    document.getElementById(elemID).innerHTML = "Stop";
    document.getElementById(elemID).setAttribute("onClick", "javascript: stopFn(" + fn.name + ", \"" + txt + "\", \"" + elemID + "\", false)");
  } else if (visible == false) {
    stopRepeat = true;
    document.getElementById("step").style.visibility = "visible";
    document.getElementById(elemID).innerHTML = txt;
    document.getElementById(elemID).setAttribute("onClick", "javascript: repeat(" + fn.name + ", \"" + elemID + "\")");
  }
};

/*
 ** findPerimeter()
 ** Returns array of perimeter cells of flooding group
 */
function findPerimeter() {
  "use strict";
  var topLeftCell = oGrid.rows[0].cells[0],
    i, j,
    bottomestCells = [],
    bottomestSoFar = [],
    rightestCells = [],
    rightestSoFar = [],
    perimeterCells = [],
    colCounter = 0,
    rowCounter = 0;

  for (i = 0; i < mGroupedCells[findGroup(topLeftCell, mGroupedCells)].length; i++) {
    if (mGroupedCells[findGroup(topLeftCell, mGroupedCells)][i].parentElement.rowIndex == rowCounter) {
      rightestSoFar[rowCounter] = 0;
      rowCounter++;
    }
  }
  for (i = 0; i < rowCounter; i++) {
    for (j = 0; j < mGroupedCells[findGroup(topLeftCell, mGroupedCells)].length; j++) {
      if (mGroupedCells[findGroup(topLeftCell, mGroupedCells)][j].parentElement.rowIndex == i) {
        if (mGroupedCells[findGroup(topLeftCell, mGroupedCells)][j].cellIndex >= rightestSoFar[i]) {
          rightestCells[i] = mGroupedCells[findGroup(topLeftCell, mGroupedCells)][j];
          rightestSoFar[i] = rightestCells[i].cellIndex;
        }
      }
    }
  }
  for (i = 0; i < mGroupedCells[findGroup(topLeftCell, mGroupedCells)].length; i++) {
    if (mGroupedCells[findGroup(topLeftCell, mGroupedCells)][i].cellIndex == colCounter) {
      bottomestSoFar[colCounter] = 0;
      colCounter++;
    }
  }
  for (i = 0; i < colCounter; i++) {
    for (j = 0; j < mGroupedCells[findGroup(topLeftCell, mGroupedCells)].length; j++) {
      if (mGroupedCells[findGroup(topLeftCell, mGroupedCells)][j].cellIndex == i) {
        if (mGroupedCells[findGroup(topLeftCell, mGroupedCells)][j].parentElement.rowIndex >= bottomestSoFar[i]) {
          bottomestCells[i] = mGroupedCells[findGroup(topLeftCell, mGroupedCells)][j];
          bottomestSoFar[i] = bottomestCells[i].parentElement.rowIndex;
        }
      }
    }
  }
  return unionArrays(rightestCells, bottomestCells);
}

/*
 ** surroundingCells(cell)
 ** Returns an array of the four neighbouring cells of the argument cell 
 */
function surroundingCells(cell) {
  "use strict";
  // surroundingCells: 0-above, 1-right, 2-below, 3-left
  var below, above, right, left, i;
  above = cell.parentElement.rowIndex != 0 ?
    oGrid.rows[cell.parentElement.rowIndex - 1].cells[cell.cellIndex] :
    null
  right = cell.cellIndex != 11 ?
    oGrid.rows[cell.parentElement.rowIndex].cells[cell.cellIndex + 1] :
    null
  below = cell.parentElement.rowIndex != 11 ?
    oGrid.rows[cell.parentElement.rowIndex + 1].cells[cell.cellIndex] :
    null
  left = cell.cellIndex != 0 ?
    oGrid.rows[cell.parentElement.rowIndex].cells[cell.cellIndex - 1] :
    null
  return [above, right, below, left]
}

/*
 ** autosolve_cycle()
 ** Cycles through the six colors
 */
function autosolve_cycle() {
  "use strict";
  changeColor(colors[lastColor % 6]);
  lastColor++;
}

/*
 ** autosolve_neighbour()
 ** Picks most frequently occuring neighbouring color
 */
function autosolve_neighbour() {
  "use strict";
  var topLeftCell = oGrid.rows[0].cells[0],
    perimeterCells = findPerimeter(),
    surrCells,
    adjacentColors = [],
    adjacentGroups = [],
    max, idx, i, j;

  /* define four cells surrounding each perimeter cell */
  /* null if on edge of grid */
  for (i = 0; i < perimeterCells.length; i++) {
    surrCells = surroundingCells(perimeterCells[i]);

    /* ascertain adjacent colors */

    /* Add cell to adjacent colors if it's:
     ** not null (i.e. off the grid), 
     ** not one of the perimeter cells, and 
     ** not within the flooding group */
    for (j = 0; j < surrCells.length; j++) {
      if (surrCells[j] != null && !searchArray(surrCells[j], perimeterCells) && !searchArray(surrCells[j], mGroupedCells[findGroup(topLeftCell, mGroupedCells)])) {
        adjacentColors.push(surrCells[j].dataset.color);
      }
    }
  }

  adjacentColors = frequency(adjacentColors);
  max = Math.max.apply(Math, adjacentColors[1]);
  idx = adjacentColors[1].indexOf(max);
  changeColor(colors[findGroup(adjacentColors[0][idx], colors)]);
}

/*
 ** autosolve_groups()
 ** Picks neighbouring color leading to highest number of cells
 */
function autosolve_groups() {
  "use strict";
  var topLeftCell = oGrid.rows[0].cells[0],
    perimeterCells = findPerimeter(),
    surrCells, i, j,
    bestShotColor, bestShotLength,
    adjacentGroups = [],
    adjacentColors = [];

  for (i = 0; i < perimeterCells.length; i++) {
    surrCells = surroundingCells(perimeterCells[i]);
    for (j = 0; j < surrCells.length; j++) {
      /* Add cell to adjacent groups if it's: 
       ** not null (i.e. off the grid)
       ** not one of the perimeter cells
       ** not in the flooding group 
       ** not already added to group of adjacent cells */
      if (surrCells[j] != null && !searchArray(surrCells[j], perimeterCells) && !searchArray(surrCells[j], mGroupedCells[findGroup(topLeftCell, mGroupedCells)]) && findGroup(surrCells[j], adjacentGroups) == -1) {
        /* Add to existing adjacent group
         ** if neighbouring cell's color is already accounted for */
        if (findColor(surrCells[j].dataset.color, adjacentGroups) != -1) {
          adjacentGroups[findColor(surrCells[j].dataset.color, adjacentGroups)] = unionArrays(adjacentGroups[findColor(surrCells[j].dataset.color, adjacentGroups)], mGroupedCells[findGroup(surrCells[j], mGroupedCells)]);
        } else {
          /* Otherwise start new color group */
          adjacentGroups.push(mGroupedCells[findGroup(surrCells[j], mGroupedCells)]);
        }
      }
    }
  }

  /* Union any adjacent groups with same colors */
  for (i = 0; i < adjacentGroups.length - 1; i++) {
    if (adjacentGroups[i][0].dataset.color == adjacentGroups[i + 1][0].dataset.color) {
      adjacentGroups[i] = unionArrays(adjacentGroups[i], adjacentGroups[i + 1]);
      adjacentGroups.splice(i + 1, 1);
    }
  }

  bestShotLength = 0;
  for (i = 0; i < adjacentGroups.length; i++) {
    console.log("adjacentGroups: " + adjacentGroups[i][0].dataset.color)
    if (adjacentGroups[i].length > bestShotLength) {
      bestShotLength = adjacentGroups[i].length;
      bestShotColor = adjacentGroups[i][0].dataset.color;
    }
  }
  console.log("bestShotColor: " + bestShotColor);
  console.log("findGroup?: " + findGroup(bestShotColor, colors));
  changeColor(colors[findGroup(bestShotColor, colors)]);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// GAMEPLAY /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/*
 ** groupCell(row_value, column_value)
 ** Groups adjacent cells of the same colour
 */
function groupCell(row, col) {
  "use strict";
  var colAfter, rowAfter,
    one, two;
  /* if current cell has no group */
  if (findGroup(oGrid.rows[row].cells[col], mGroupedCells) == -1) {
    /* create group and add cell to it */
    mGroupedCells.push([oGrid.rows[row].cells[col]]);
  }

  for (colAfter = col + 1; colAfter < gridSize; colAfter++) {
    if (oGrid.rows[row].cells[col].dataset.color == oGrid.rows[row].cells[colAfter].dataset.color) {

      /* if new cell has group */
      if (findGroup(oGrid.rows[row].cells[colAfter], mGroupedCells) != -1) {
        /* concat groups & get rid of the latter */
        one = findGroup(oGrid.rows[row].cells[col], mGroupedCells);
        two = findGroup(oGrid.rows[row].cells[colAfter], mGroupedCells);
        mGroupedCells[one] = unionArrays(mGroupedCells[one], mGroupedCells[two]);
        if (one != two) {
          mGroupedCells.splice(two, 1);
        }

      } else {
        /* add to group */
        one = findGroup(oGrid.rows[row].cells[col], mGroupedCells);
        two = oGrid.rows[row].cells[colAfter];
        mGroupedCells[one].push(two);
      }
    } else {
      /* no match, so break out of current cell */
      break;
    }
  }

  for (rowAfter = row + 1; rowAfter < gridSize; rowAfter++) {

    if (oGrid.rows[row].cells[col].dataset.color == oGrid.rows[rowAfter].cells[col].dataset.color) {

      /* if new cell has group */
      if (findGroup(oGrid.rows[rowAfter].cells[col], mGroupedCells) != -1) {
        /* concat groups & get rid of the latter */
        one = findGroup(oGrid.rows[row].cells[col], mGroupedCells);
        two = findGroup(oGrid.rows[rowAfter].cells[col], mGroupedCells);
        mGroupedCells[one] = unionArrays(mGroupedCells[one], mGroupedCells[two]);
        if (one != two) {
          mGroupedCells.splice(two, 1);
        }

      } else {
        /* add to group */
        one = findGroup(oGrid.rows[row].cells[col], mGroupedCells);
        two = oGrid.rows[rowAfter].cells[col];
        mGroupedCells[one].push(two);
      }

    } else {
      /* no match, so break out of current cell */
      break;
    }
  }
}

/* 
 ** changeColor(player_selected_color)
 ** Handles each turn of the game
 ** Propagates color change across the grid
 */
function changeColor(color) {
  "use strict";
  var idx, row, col, topLeftCell;
  topLeftCell = oGrid.rows[0].cells[0];

  /* if chosen color is already selected */
  if (topLeftCell.dataset.color == color[0]) {
    return;
  } else {

    /* keep track of turns */
    turns++;
    document.getElementById("stepCounter").innerHTML = turns + "/22";

    /* set color of cells in flooding group to player selected color */
    for (idx = 0; idx < mGroupedCells[findGroup(topLeftCell, mGroupedCells)].length; idx++) {
      mGroupedCells[findGroup(topLeftCell, mGroupedCells)][idx].dataset.color = color[0];
      mGroupedCells[findGroup(topLeftCell, mGroupedCells)][idx].style.backgroundColor = color[1];
    }
    colorCount = 0;

    for (row = 0; row < gridSize; row++) {
      for (col = 0; col < gridSize; col++) {

        /* regroup cells based on flooding */
        groupCell(row, col);

        /* count the number of cells which are the flooding color */
        if (oGrid.rows[row].cells[col].dataset.color == color[0]) {
          colorCount++;
        }

        /* display percentage */
        document.getElementById("percentage").innerHTML = Math.round(mGroupedCells[findGroup(topLeftCell, mGroupedCells)].length * 100 / (gridSize * gridSize)) + "%";
        document.getElementById("percentage").style.color = color[1];
      }
    }

    setTimeout(function () {
      /* if the whole grid is one color, player has won */
      if (colorCount == (gridSize * gridSize)) {
        window.alert("Congratulations! You've won, in " + turns + " steps!");
        location.reload();
      } else {
        if (turns >= 22) {
          window.alert("You're out of turns!");
          location.reload();
        }
      }
    }, 500);
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// INITIALISATION ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

/*
 ** initialiseGrid()
 ** Randomises grid & calls groupCell()
 ** Runs on window load
 */
function initialiseGrid() {
  "use strict";
  oGrid = document.getElementById("grid");
  oButtons = [document.getElementById("button1"),
              document.getElementById("button2"),
              document.getElementById("button3"),
              document.getElementById("button4"),
              document.getElementById("button5"),
              document.getElementById("button6")];
  var row, col, color, rowAfter, colAfter, i, x, y, one, two, randomColor, buttonColor, buttonID;
  turns = 0;

  /* set button colors & function */
  for (buttonID = 0; buttonID < oButtons.length; buttonID++) {
    oButtons[buttonID].style.backgroundColor = colors[buttonID][1];
    buttonColor = colors[buttonID];

    oButtons[buttonID].onclick =
      (function (buttonColor) {
        return function () {
          changeColor(buttonColor)
        }
      })(buttonColor);
  }

  /* randomise cells */
  for (row = 0; row < gridSize; row++) {
    for (col = 0; col < gridSize; col++) {
      randomColor = randomChoice(colors);
      oGrid.rows[row].cells[col].dataset.color = randomColor[0];
      oGrid.rows[row].cells[col].style.backgroundColor = randomColor[1];
    }
  }

  /* iterate over whole grid */
  for (row = 0; row < gridSize; row++) {
    for (col = 0; col < gridSize; col++) {
      groupCell(row, col);
    }
  }
  if (debug) {
    for (i = 0; i < mGroupedCells.length; i++) {
      mGroupedCells[i][0].innerHTML = "x";
    }
  }

}

lastColor = 0;
window.onload = initialiseGrid;
